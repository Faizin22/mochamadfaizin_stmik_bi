/* 
Nama    : Mochamad Faizin Akbar Afandi
NIM     : 2018120051
MK      : Pemrograman 3
Prodi   : Sistem Informasi
*/

// Soal No.1
console.log('Soal No.1')
function range(startNum, finishNum){
    let s = startNum
    let f = finishNum
    let arr = []
    if (startNum < finishNum){
        for (s;s<=f; s++){
            arr.push(s)
        }
    }else if (startNum > finishNum){
        for (s;s>=f; s--){
            arr.push(s)
        }
    }else{
        arr = -1
    }
    return arr
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log()
// Soal No.2
console.log('Soal No.2')
function rangeWithStep(startNum, finishNum, step){
    let s = startNum
    let f = finishNum
    let arr = []
    if (startNum < finishNum){
        for(s;s<=f;s+=step){
            arr.push(s)
        }
    }else{
        for(s;s>=f;s-=step){
            arr.push(s)
        }
    }
    return arr
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log()
// Soal No.3
console.log('Soal No.3')
function sum(startNum, finishNum, step){
    let s = startNum
    let f = finishNum
    let total = 0
    let arr = []
    if (step == undefined){
        step = 1
    }
    if (startNum < finishNum){
        for(s;s<=f;s+=step){
        arr.push(s)
        total += s
    }
    }else if (startNum > finishNum){
        for(s;s>=f;s-=step){
            arr.push(s)
            total += s
        }
    }else if (startNum >= 0 && finishNum == undefined) {
        total = s
    }
return total
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log()
// Soal No.4
console.log('Soal No.4')
function dataHandling(){
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]

    ] 
    i=0
    j=0
    p=input.length
    for (i;i<p;i++){
        console.log('Nomor ID: ',input[i][0])
        console.log('Nama Lengkap: ',input[i][1])
        console.log('TTL: ',input[i][2],input[i][3])
        console.log('Hobi: ',input[i][4])
    }
    return ' '
}
console.log(dataHandling())

console.log()
// Soal No.5
console.log('Soal No.5')
function balikKata(kata){
    var balik = '';
    var i = kata.length-1
    for (var i; i >= 0; i--){
       balik = balik + kata[i];
}
return balik;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log()
//Soal No.6
console.log('Soal No.6')
//step 1
function dataHandling2(data){
    //step 2
    data.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung")
    data.splice(4, 1, "Pria", "SMA Internasional Metro")
    console.log(data)
    //step3
    const tanggal = data[3].split('/')
    var bulan = tanggal[1]
    switch(bulan){
        case '01': {console.log('Januari');break;}
        case '02': {console.log('Februari');break;}
        case '03': {console.log('Maret');break;}
        case '04': {console.log('April');break;}
        case '05': {console.log('Mei');break;}
        case '06': {console.log('Juni');break;}
        case '07': {console.log('Juli');break;}
        case '08': {console.log('Agstus');break;}
        case '09': {console.log('September');break;}
        case '10': {console.log('Oktober');break;}
        case '11': {console.log('November');break;}
        case '12': {console.log('Desember');break;}
        default: {console.log('tidak dapat menetukan bulan');}}
    //Step 4 & 5
    const gabung = tanggal.join('-')
    const susun = tanggal.sort(function(value1, value2){return value2 - value1})
    console.log(susun)
    console.log(gabung)
    //Step 6
    var batas = data[1].slice(0,15)
    console.log(batas)
}

var input= ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(input)