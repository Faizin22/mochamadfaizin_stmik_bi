/* 
Nama    : Mochamad Faizin Akbar Afandi
NIM     : 2018120051
MK      : Pemrograman 3
Prodi   : Sistem Informasi
*/

// Soal No.1 
console.log('Soal No.1')
const golden = goldenFunction = () => {
    console.log("this is golden!!")
}

golden()

console.log()
// Soal No.2
console.log('Soal No.2')
const newFunction = (firstName, lastName) => {
    return {
        firstName, 
        lastName,
        fullName(){
            console.log(`${firstName} ${lastName}`)
        }
    }
  }
  //Driver Code 
  newFunction("William", "Imoh").fullName()
  
console.log()
// Soal No.3
console.log('Soal No.3')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {
    firstName,
    lastName,
    destination,
    occupation,
    spell
} = newObject

console.log(firstName, lastName, destination, occupation, spell)

console.log()
// Soal No.4
console.log('Soal No.4')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

console.log()
// Soal No.5
console.log('Soal No.5')
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} pdo eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 
