/* 
Nama    : Mochamad Faizin Akbar Afandi
NIM     : 2018120051
MK      : Pemrograman 3
Prodi   : Sistem Informasi
*/


var readBooks = require('./callback.js')
 
const books = [
    { name: "LOTR", timeSpent: 3000 },
    { name: "Fidas", timeSpent: 2000 },
    { name: "Kalkulus", timeSpent: 4000 },
  ];
  
  jmlBuku = books.length;
  
  const render = (time, index) => {
    readBooks(time, books[index], (siswaWaktu) => {
      waktu = siswaWaktu;
      jmlBuku = jmlBuku - 1;
      if (jmlBuku > 0) {
        render(waktu, index + 1, jmlBuku);
      }
    });
  };
  
  render(10000 , 0);