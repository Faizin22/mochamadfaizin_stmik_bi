/* 
Nama    : Mochamad Faizin Akbar Afandi
NIM     : 2018120051
MK      : Pemrograman 3
Prodi   : Sistem Informasi
*/

// Soal No.1 
console.log('Soal No.1')
function arrayToObject(arr){
    var bio = {}
    var now = new Date()
    var thisYear = now.getFullYear()
    i = 0
    p = arr.length
    for (i;i<p;i++){
        bio.FirstName = arr[i][0]
        bio.LastName = arr[i][1]
        bio.gender = arr[i][2]
        if (arr[i][3] <= thisYear){
        umur = thisYear - arr[i][3]
        }else{ umur = 'Invalid Birth Year'}
        bio.age = umur

        console.log(i+1+'.', bio.FirstName, bio.LastName, bio)
    }
}
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
// Error case 
arrayToObject([]) // ""

console.log()
// Soal No.2 
console.log('Soal No.2')
function shoppingTime(memberId, money) {
    if (memberId == null || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000 || money == null) {
        return "Mohon maaf, uang tidak cukup"
    } else {
        var harga = {
            "Sepatu Stacattu": 1500000,
            "Baju Zoro": 500000,
            "Baju H&N": 250000,
            "Sweater Uniklooh": 175000,
            "Casing Handphone": 50000,
        }
        var produk = Object.keys(harga)
        var listPurchased = []
        var changeMoney = money
        for (var i = 0; i < 5; i++) {
            if (changeMoney-harga[produk[i]] >= 0) {
                listPurchased.push(produk[i])
                changeMoney -= harga[produk[i]]
            }
        }
        var pembeli = {
            memberId: memberId,
            money: money,
            listPurchased: listPurchased,
            changeMoney: changeMoney
        }
        return pembeli
    }
  }

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime()); 

console.log()
// Soal No.3
console.log('Soal No.3')
function naikAngkot(listPenumpang){
    rute = ['A', 'B', 'C', 'D', 'E', 'F']
    var tarifAngkot = []
    var i=0
    
    for (i;i < listPenumpang.length;i++){
        var titik = 0
        var jalan = false
        if(listPenumpang[i][1] != listPenumpang[i][2]){
            var j=0
            for (j;j<rute.length;j++){
                if (jalan){
                    titik++
                } 
                if (rute[j]==listPenumpang[i][1] || rute[j]==listPenumpang[i][2]){
                    jalan = !jalan
                }
            }
        }
        var penumpang = {
            penumpang: listPenumpang[i][0],
  		naikDari: listPenumpang[i][1],
  		tujuan: listPenumpang[i][2],
  		bayar: 2000*titik
        }
        tarifAngkot.push(penumpang)
    }
    return tarifAngkot
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
