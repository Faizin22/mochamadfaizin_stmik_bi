/* 
Nama    : Mochamad Faizin Akbar Afandi
NIM     : 2018120051
MK      : Pemrograman 3
Prodi   : Sistem Informasi
*/

// Soal No.1
console.log('Soal No.1');
console.log('LOOPING PERTAMA');
var loop=2;
while (loop <= 20) {
    
    console.log(loop,'- I love coding')
    loop += 2;
}
console.log('LOOPING KEDUA');
var loop=20;
while (loop >= 2) {
    console.log(loop,'- I will become a mobile developer');
    loop -= 2;
}

console.log()
// Soal No.2
console.log('Soal No.2');
for(var angka = 1;angka <= 20; angka++){
    if (angka % 2 == 1 && angka % 3 == 0){
        console.log(angka,'- I Love Coding')
    }else if (angka % 2 == 1){ 
        console.log(angka,'- Santai')
    }else if(angka % 2 == 0){
        console.log(angka,'- Berkualitas')
    }

}

console.log()
//Soal No.3
console.log('Soal No.3');
    for(var tagar = 0; tagar <= 3; tagar++){
        var pagar = '#'
        for (var tagar1 = 0; tagar1 <= 6; tagar1++){
            pagar+='#'
        }
        console.log(pagar)
    }
    
console.log()
// Soal No.4
console.log('Soal No.4')
var tangga = '#'
for (var tangga1 = 0; tangga1 <= 6; tangga1++){
    console.log(tangga)
    tangga+='#'
}

console.log()
// Soal No.5
console.log('Soal No.5');
for(var hitam1 = 0; hitam1 <= 7; hitam1++){
    var putih=' '
    for (var hitam2 = 0; hitam2 <= 7; hitam2++){
        if(hitam1 % 2 == 0){
           if (hitam2 % 2 == 0){
                putih+=' '
            }else{
                putih+='#'
            }
        }else if(hitam2 % 2 == 0){
            putih +='#'
        }else {
            putih+=' '
        }
    }
    console.log(putih)
}
